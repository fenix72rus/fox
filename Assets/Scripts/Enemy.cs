﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public GameObject _gameObject;
    public RectTransform Rect;

    public float speed;
    public BoardMover boardMover;

    void FixedUpdate()
    {
        if (!GameManager.gameManager.gameStarted) return;

        Rect.Translate(new Vector3(0, -boardMover.moveSpeed * speed, 0) * Time.deltaTime);
        if (Rect.position.y <= -1100)
        {
            Respawn();
        }
    }

    private void Respawn()
    {
        gameObject.SetActive(false);
    }
}
