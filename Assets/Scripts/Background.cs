﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    [SerializeField] private BoardMover boardMover;

    private RectTransform thisRect;
    private Vector3 startPosition;

    private void Awake()
    {
        thisRect = GetComponent<RectTransform>();
        startPosition = thisRect.localPosition;
    }

    private void FixedUpdate()
    {
        if (!GameManager.gameManager.gameStarted) return;

        thisRect.Translate(new Vector3(0, -boardMover.moveSpeed / 2f, 0) * Time.deltaTime);
        if (thisRect.localPosition.y <= -2200)
        {
            Respawn();
        }
    }

    private void Respawn()
    {
        thisRect.localPosition = new Vector3(startPosition.x, 2200, startPosition.z);
    }
}
