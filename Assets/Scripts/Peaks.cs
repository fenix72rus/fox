﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Peaks : MonoBehaviour
{
    [SerializeField] private float speedRotate;
    private RectTransform rect, rectChild;

    private void Start()
    {
        rect = GetComponent<RectTransform>();
        rectChild = rect.GetChild(0).GetComponent<RectTransform>();
    }

    private void FixedUpdate()
    {
        if (GameManager.gameManager.pause) return;

        rect.Rotate(new Vector3(0, 0, 1 * speedRotate * Time.deltaTime));
        rectChild.Rotate(new Vector3(0, 0, -1 * speedRotate * Time.deltaTime));
    }
}
