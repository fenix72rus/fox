﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    private Vector3 startTouchPosition;

    void Update()
    {
        if (GameManager.gameManager.gameStarted == false) return;
        if (GameManager.gameManager.pause) return;

        var touches = Input.touches;

        if(touches.Length > 0)
        {
            foreach(var touch in touches)
            {
                if(touch.phase == TouchPhase.Began)
                {
                    startTouchPosition = touch.position;
                }
                if(touch.phase == TouchPhase.Ended)
                {
                    if (touch.position.x > startTouchPosition.x)
                    {
                        GameManager.gameManager.Move(Player.Side.right);
                    }
                    else
                    {
                        GameManager.gameManager.Move(Player.Side.left);
                    }
                }
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                startTouchPosition = Input.mousePosition;
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (Input.mousePosition.x > startTouchPosition.x)
                {
                    GameManager.gameManager.Move(Player.Side.right);
                }
                else
                {
                    GameManager.gameManager.Move(Player.Side.left);
                }
            }
        }
    }
}
