﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyYellow : Enemy
{
    //[SerializeField] private Bullet[] bullets;
    //private IEnumerator shoot;
    [SerializeField] private Animator animator;
    //private bool paused;
    

    /*private void OnEnable()
    {
        shoot = Shoot();
        StartCoroutine(shoot);
    }

    private void OnDisable()
    {
        if (shoot != null) StopCoroutine(Shoot());
        foreach (var bullet in bullets) bullet.gameObject.SetActive(false);
    }

    private IEnumerator Shoot()
    {
        yield return new WaitForSeconds(4f);
        if(!paused)
        {
            foreach (var bullet in bullets)
            {
                bullet.gameObject.SetActive(true);
                bullet.Shoot();
            }
        }
        yield return new WaitForSeconds(3f);
        if(!paused)
        {
            foreach (var bullet in bullets)
            {
                bullet.gameObject.SetActive(false);
                bullet.ReturnToPool();
            }
        }
        shoot = Shoot();
        StartCoroutine(shoot);
    }*/
    public void Pause(bool pause)
    {
        animator.enabled = !pause;
        //paused = pause;
    }

    public void ShootAudio()
    {
        AudioManager.audioManager.ShootAudio();
    }
}
