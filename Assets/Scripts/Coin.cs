﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Coin : MonoBehaviour
{
    public GameObject _gameObject;
    public RectTransform Rect, ParentRect;

    [SerializeField] private BoardMover boardMover;

    void FixedUpdate()
    {
        if (!GameManager.gameManager.gameStarted) return;

        ParentRect.Translate(new Vector3(0, -boardMover.moveSpeed, 0) * Time.deltaTime);
        if (ParentRect.position.y <= -1100)
        {
            Respawn();
        }
    }

    private void Respawn()
    {
        gameObject.SetActive(false);
    }
}
