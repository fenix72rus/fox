﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManager;
    public bool gameStarted, pause;
    public int coins, allCoins;

    [SerializeField] private Image respawFox;
    [SerializeField] private GameObject startPanel, pausePanel, respawnPanel, deathPanel, adsPanel;
    [SerializeField] private EnemyRed enemyRed;
    [SerializeField] private TextMeshProUGUI coinsText, scoreText, bestScoreText, menuBestScore, menuCoins, deathScore;
    [SerializeField] private BoardMover[] boards; 
    [SerializeField] private Player player;
    [SerializeField] private float modifire = 1;

    private int score, bestScore;
    private float startSpeed, pauseModifire;
    private IEnumerator respawn, scoreCorutine;

    private void Awake()
    {
        gameManager = this;
        startSpeed = boards[0].moveSpeed;
    }

    public void AdsPanel(bool status)
    {
        adsPanel.SetActive(status);
    }

    private void Start()
    {
        menuBestScore.text = Load("Best").ToString();
        menuCoins.text = Load("Coins").ToString();
    }

    private void FixedUpdate()
    {
        if (!gameStarted) return;

        if(!pause) modifire += Time.deltaTime / 250f;
        var speed = startSpeed * modifire;
        boards[0].moveSpeed = speed;
        boards[1].moveSpeed = speed;
    }

    public void Move(Player.Side side)
    {
        player.Move(side);
    }

    public void StartGame()
    {
        if (respawn != null) StopCoroutine(respawn);
        gameStarted = true;
        player.StartGame();
        boards[0].Restart();
        boards[1].Restart();
        boards[1].RespawnPeaks();
        if (scoreCorutine == null)
        {
            scoreCorutine = Score();
            StartCoroutine(scoreCorutine);
        }
        startPanel.SetActive(false);
        deathPanel.SetActive(false);
        pausePanel.SetActive(false);
        respawnPanel.SetActive(false);
        bestScore = Load("Best");
        bestScoreText.text = Load("Best").ToString();
        coins = 0;
        coinsText.text = coins.ToString();
        pause = false;
        modifire = 1;
        score = 0;
        scoreText.text = score.ToString();
        allCoins = Load("Coins") + coins;
        Save("Coins", allCoins);
        Save("Best", bestScore);
    }

    public void EndGame()
    {
        Pause(false);
        respawn = RespawnDelay();
        allCoins = Load("Coins") + coins;
        Save("Coins", allCoins);
        Save("Best", bestScore);
        StartCoroutine(respawn);
    }

    private IEnumerator RespawnDelay()
    {
        respawnPanel.gameObject.SetActive(true);
        respawFox.fillAmount = 1;
        yield return new WaitForSeconds(0.5f);
        respawFox.DOFillAmount(0, 7f);
        yield return new WaitForSeconds(7f);
        respawnPanel.gameObject.SetActive(false);
        gameStarted = false;
        deathPanel.SetActive(true);
        deathScore.text = score.ToString();
    }

    private void EndPanel()
    {
    }

    public void AddCoin()
    {
        AudioManager.audioManager.TakeCoin();
        coins++;
        coinsText.text = coins.ToString();
        StartCoroutine(AddCoinAnimation());
    }

    public IEnumerator Score()
    {
        yield return new WaitForSeconds(1f);
        if(!pause)
        {
            score++;
            scoreText.text = score.ToString();
            if (score > bestScore)
            {
                bestScore = score;
                bestScoreText.text = bestScore.ToString();
            }
        }
        StartCoroutine(Score());
    }

    public void Pause(bool showPanel)
    {
        if (pause) return;

        pauseModifire = modifire;
        modifire = 0;
        if(enemyRed.gameObject.activeSelf) enemyRed.Pause(true);
        if(showPanel) pausePanel.SetActive(true);
        pause = true;
    }

    public void AdsUnDeathPause()
    {
        modifire = pauseModifire;
        if (enemyRed.gameObject.activeSelf) enemyRed.Pause(false);
        pausePanel.SetActive(false);
        deathPanel.SetActive(false);
        respawnPanel.SetActive(false);
        StartCoroutine(player.DamageDelay());
        if (respawn != null) StopCoroutine(respawn);
        player.HealAll();

        pause = false;
    }

    public void AdsWatched()
    {
        AdsPanel(false);
        if(deathPanel.activeSelf)
        {
            allCoins = Load("Coins") + coins;
            Save("Coins", allCoins);
            coins = 0;
        }
        else
        {
            if (player.currentHealth <= 0)
            {
                Debug.Log("Death ad");
                AdsUnDeathPause();
                return;
            }
            else
            {
                Debug.Log("Pause ad");
                if (player.currentHealth == player.maxHealth) return;
                player.Heal();
            }
        }
    }

    public void GoToMenu()
    {
        allCoins = Load("Coins") + coins;
        Save("Coins", allCoins);
        Save("Best", bestScore);
        startPanel.SetActive(true);
        menuBestScore.text = Load("Best").ToString();
        menuCoins.text = Load("Coins").ToString();
        gameStarted = false;
    }

    public void UnPause()
    {
        modifire = pauseModifire;
        if (enemyRed.gameObject.activeSelf) enemyRed.Pause(false);
        pausePanel.SetActive(false);
        if (respawn != null) StopCoroutine(respawn);
        pause = false;
    }

    private IEnumerator AddCoinAnimation()
    {
        var coinTextRect = coinsText.rectTransform;
        var coinStandartScale = coinTextRect.localScale;
        coinTextRect.DOScale(new Vector3(coinStandartScale.x * 1.5f, coinStandartScale.y * 1.5f, coinStandartScale.z * 1.5f), 0.5f);
        yield return new WaitForSeconds(0.5f);
        coinTextRect.DOScale(coinStandartScale, 0.5f);
    }
    public void Save(string key, int valume)
    {
        PlayerPrefs.SetInt(key, valume);
        PlayerPrefs.Save();
    }

    public int Load(string key)
    {
        if (PlayerPrefs.HasKey(key))
        {
            return PlayerPrefs.GetInt(key);
        }
        else
        {
            return 0;
        }
    }
}
