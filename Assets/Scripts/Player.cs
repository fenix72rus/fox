﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public enum Side
    { 
        left,
        right
    }

    public int maxHealth = 3;
    [SerializeField] private Image[] healthBar;

    public int currentHealth;
    private Vector3 standartScale;
    private RectTransform _transform;
    private Image image;
    private Side currentSide;
    private bool isMoving, noDamage;

    private void Awake()
    {
        currentSide = Side.right;
        _transform = GetComponent<RectTransform>();
        image = GetComponent<Image>();
        standartScale = _transform.localScale;
    }

    public void StartGame()
    {
        currentHealth = maxHealth;
        foreach(var health in healthBar)
        {
            health.color = Color.white;
        }
        image.color = new Color32(255, 255, 255, 255);
        currentSide = Side.right;
        _transform.localPosition = new Vector3(335f, _transform.localPosition.y, _transform.localPosition.z);
    }

    public void Move(Side side)
    {
        if (isMoving) return;
        if (side == currentSide) return;

        AudioManager.audioManager.Move();
        var movePosition = 0f;
        if (side == Side.left)
        {
            movePosition = -335f;
        }
        else
        {
            movePosition = 335f;
        }
        var duration = 0.3f;
        _transform.DOLocalMoveX(movePosition, duration);
        currentSide = side;
        StartCoroutine(MoveDuration(duration));
    }

    private IEnumerator MoveDuration(float duration)
    {
        isMoving = true;
        _transform.DOScale(new Vector3(standartScale.x, standartScale.y * 0.75f, standartScale.z), duration / 2);
        yield return new WaitForSeconds(duration / 2f);
        _transform.DOScale(standartScale, duration / 2);
        yield return new WaitForSeconds(duration / 2);
        isMoving = false;
    }

    private void TakeDamage()
    {
        if (GameManager.gameManager.gameStarted == false) return;

        AudioManager.audioManager.TakeDamage();
        StartCoroutine(DamageDelay());
        currentHealth -= 1;
        if (currentHealth < 0) currentHealth = 0;
        Debug.Log(healthBar.Length + " " + (currentHealth));
        healthBar[currentHealth].color = new Color32(255, 255, 255, 150);
        if(currentHealth <= 0)
        {
            //image.color = new Color32(255, 255, 255, 0);
            GameManager.gameManager.EndGame();
        }
        else
        {
            StartCoroutine(DamageAnimation());
        }
    }

    public void HealAll()
    {
        currentHealth = maxHealth;
        foreach(var health in healthBar)
        {
            health.color = new Color32(255, 255, 255, 255);
        }
    }

    public void Heal()
    {
        currentHealth++;
        if (currentHealth > maxHealth) currentHealth = maxHealth;
        healthBar[currentHealth - 1].color = new Color32(255, 255, 255, 255);
    }

    private IEnumerator DamageAnimation()
    {
        image.DOFade(0, 0.1f);
        yield return new WaitForSeconds(0.2f);
        image.DOFade(255, 0.1f);
        yield return new WaitForSeconds(0.2f);
        image.DOFade(0, 0.1f);
        yield return new WaitForSeconds(0.2f);
        image.DOFade(255, 0.1f);
        yield return new WaitForSeconds(0.2f);
    }

    public IEnumerator DamageDelay()
    {
        noDamage = true;
        yield return new WaitForSeconds(2f);
        noDamage = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.TryGetComponent<Coin>(out Coin coin))
        {
            GameManager.gameManager.AddCoin();
            coin._gameObject.SetActive(false);
            return;
        }

        if (noDamage) return;

        if (collision.gameObject.TryGetComponent<Peaks>(out Peaks peaks))
        {
            TakeDamage();
            return;
        }
        if(collision.gameObject.TryGetComponent<Enemy>(out Enemy enemy))
        {
            TakeDamage();
            return;
        }
        if(collision.gameObject.TryGetComponent<Bullet>(out Bullet bullet))
        {
            TakeDamage();
            return;
        }
    }
}
