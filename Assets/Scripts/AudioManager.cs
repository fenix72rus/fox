﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    public static AudioManager audioManager;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip pressButtonAudio, rockAudio, moveAudio, damageAudio, coinAudio, shootAudio;
    [SerializeField] private Image audioImage;
    [SerializeField] private Sprite onAudio, offAudio;

    private void Awake()
    {
        audioManager = this;
    }

    public void ChangeAudioStatus()
    {
        if(audioSource.volume > 0)
        {
            audioSource.volume = 0f; 
            audioImage.sprite = offAudio;
        }
        else
        {
            audioSource.volume = 1f;
            audioImage.sprite = onAudio;
        }    
    }

    public void PressButton()
    {
        if (!GameManager.gameManager.gameStarted) return;
        audioSource.PlayOneShot(pressButtonAudio);
    }

    public void TakeDamage()
    {
        if (!GameManager.gameManager.gameStarted) return;
        audioSource.PlayOneShot(damageAudio);
    }

    public void TakeCoin()
    {
        if (!GameManager.gameManager.gameStarted) return;
        audioSource.PlayOneShot(coinAudio);
    }

    public void Move()
    {
        if (!GameManager.gameManager.gameStarted) return;
        audioSource.PlayOneShot(moveAudio);
    }

    public void Rock()
    {
        if (!GameManager.gameManager.gameStarted) return;
        audioSource.PlayOneShot(rockAudio);
    }

    public void ShootAudio()
    {
        if (!GameManager.gameManager.gameStarted) return;
        audioSource.PlayOneShot(shootAudio);
    }
}
