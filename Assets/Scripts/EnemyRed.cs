﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemyRed : Enemy
{
    private Tween moveTween;
    private IEnumerator move;

    private void OnEnable()
    {
        if (moveTween != null) moveTween.Kill();
        move = Move();
        StartCoroutine(move);
        if (Random.Range(0, 2) == 0)
        {
            Rect.localPosition = new Vector3(-335, Rect.localPosition.y, Rect.localPosition.z);
        }
        else
        {
            Rect.localPosition = new Vector3(335, Rect.localPosition.y, Rect.localPosition.z);
        }
    }

    private void OnDisable()
    {
        if (moveTween != null) moveTween.Kill();
        StopCoroutine(move);
    }

    public void Pause(bool pause)
    {
        if(pause)
        {
            Debug.Log("Pause");
            moveTween.Pause();
            moveTween.Kill();
            moveTween.Kill(true);
            if(move != null) StopCoroutine(move);
        }
        else
        {
            if(gameObject.activeSelf)
            {
                move = Move();
                StartCoroutine(move);
            }
        }
    }

    private IEnumerator Move()
    {
        if(Rect.localPosition.x > 0)
        {
            Rect.DOLocalMoveX(-300, 4f);
        }
        else
        {
            Rect.DOLocalMoveX(300, 4f);
        }
        yield return new WaitForSeconds(4f);
        move = Move();
        StartCoroutine(move);
    }
}
