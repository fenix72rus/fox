﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardMover : MonoBehaviour
{
    public float moveSpeed = 1f;
    public bool gameStarted;

    [SerializeField] private Enemy[] enemies;
    [SerializeField] private Coin coin;
    [SerializeField] private GameObject[] peaksLeft, peaksRight;
    [SerializeField] private RectTransform thisRect;
    private Vector3 startPosition;
    private Enemy currentEnemy;

    private void Awake()
    {
        startPosition = thisRect.localPosition;
    }

    private void FixedUpdate()
    {
        if (!GameManager.gameManager.gameStarted) return;

        thisRect.Translate(new Vector3(0, -moveSpeed, 0) * Time.deltaTime);
        if(thisRect.localPosition.y <= -2219)
        {
            Respawn();
        }
    }

    public void Restart()
    {
        foreach (var peak in peaksRight)
        {
            peak.SetActive(false);
        }
        foreach (var peak in peaksLeft)
        {
            peak.SetActive(false);
        }
        foreach(var enemy in enemies)
        {
            enemy._gameObject.SetActive(false);
        }
        thisRect.localPosition = startPosition;
        coin._gameObject.SetActive(false);
    }

    public void RespawnPeaks()
    {
        for (int i = 0; i < peaksLeft.Length; i++)
        {
            if (Random.Range(0, 3) > 0)
            {
                if (Random.Range(0, 2) == 0)
                {
                    peaksLeft[i].SetActive(true);
                }
                else
                {
                    peaksRight[i].SetActive(true);
                }
            }
            else
            {
                Debug.Log("coin");
                if (!coin._gameObject.activeSelf)
                {
                    if (thisRect.localPosition.y > 0)
                    {
                        var spawnPosition = new Vector3(Random.Range(-360, 360), peaksLeft[i].GetComponent<RectTransform>().position.y, 0);
                        coin.ParentRect.localPosition = spawnPosition;
                        coin._gameObject.SetActive(true);
                    }
                }
            }
            foreach (var enemy in enemies)
            {
                if (enemy._gameObject.activeSelf) return;
            }
            if(Random.Range(0,2) == 0)
            {
                var enemyID = Random.Range(0, enemies.Length);
                if (enemyID == 0) AudioManager.audioManager.Rock();
                currentEnemy = enemies[enemyID];
                if (!currentEnemy._gameObject.activeSelf)
                {
                    if (thisRect.localPosition.y > 0)
                    {
                        var spawnPosition = new Vector3(Random.Range(-360, 360), peaksLeft[i].GetComponent<RectTransform>().position.y, 0);
                        currentEnemy.Rect.localPosition = spawnPosition;
                        currentEnemy._gameObject.SetActive(true);
                    }
                }
            }
        }
    }

    private void Respawn()
    {
        foreach(var peak in peaksRight)
        {
            peak.SetActive(false);
        }
        foreach (var peak in peaksLeft)
        {
            peak.SetActive(false);
        }
        thisRect.localPosition = new Vector3(startPosition.x, 2200, startPosition.z);
        RespawnPeaks();
    }
}
